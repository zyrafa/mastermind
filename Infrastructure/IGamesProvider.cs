namespace Infrastructure
{
    public interface IGamesProvider
    {
        string[] GetGameNames();
        IGame this[int n] { get; }
        IGame this[string n] { get; }
    }
}