﻿using System;
using GameLibrary;
using Infrastructure;

namespace DodatkowaPraca
{
    internal class GameFlow
    {
        private readonly IScoreManager _scoreManager;
        private readonly IGamesProvider _gamesProvider;

        public GameFlow(IGamesProvider gamesProvider, IScoreManager scoreManager)
        {
            _gamesProvider = gamesProvider;
            _scoreManager = scoreManager;
        }

        public void Run()
        {
            Console.Clear();
            Console.WriteLine("Podaj swoje imie:");
            var name = Console.ReadLine();

            Console.WriteLine("Wybierz grę:");
            foreach (var gameName in _gamesProvider.GetGameNames())
            {
                Console.WriteLine($"- {gameName}");
            }

            var game = _gamesProvider[Console.ReadLine()];

            bool result;
            do
            {
                Console.Write("Zgadnij: ");
                var bet = Console.ReadLine();
                string answer;
                result = game.Guess(bet, out answer);

                Console.WriteLine(answer);
            } while (!result);

            Console.WriteLine($"Wynik: {game.Result}");

            var highscoreEntry = new HighscoreEntry(name, game.Result);
            _scoreManager.Add(highscoreEntry);

            Console.WriteLine("\nNAJLEPSZE WYNIKI:");
            foreach (var entry in _scoreManager.List())
            {
                Console.WriteLine(entry);
            }
        }
    }
}
