﻿using System.Linq;
using Infrastructure;

namespace GameLibrary
{
    public class GamesProvider : IGamesProvider
    {
        private readonly IGame[] _games ;

        public GamesProvider(IGame[] games)
        {
            this._games = games;
        }

        public string[] GetGameNames()
        {
            return _games.Select(g => g.Name).ToArray();
        }

        public IGame this[int n]
        {
            get { return _games[n]; }
        }

        public IGame this[string n]
        {
            get { return _games.First(g => g.Name == n); }
        }
    }
}
